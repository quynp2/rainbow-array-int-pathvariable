package com.devcamp.arrayfilterinputrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.arrayfilterinputrestapi.service.ArrayInputService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ArrayInputController {
    @Autowired
    private ArrayInputService arrayInoInputService;

    @GetMapping("/array-int-request-query")
    public ArrayList<Integer> getPosGreaterThan(@RequestParam(required = true, name = "pos") int pos) {
        return arrayInoInputService.FilterLarger(pos);

    }

    @GetMapping("/array-int-param/{index}")
    public int getElementbyId(@PathVariable(required = true) int index) {
        return arrayInoInputService.GetNumberByIndex(index);
    }

}
