package com.devcamp.arrayfilterinputrestapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class ArrayInputService {
    private int[] rainbow = { 1, 23, 32, 43, 54, 65, 86, 10, 15, 16, 18 };

    public ArrayList<Integer> FilterLarger(int pos) {
        ArrayList<Integer> result = new ArrayList<>();
        for (Integer number : rainbow) {
            if (pos < number) {
                result.add(number);
            }

        }
        return result;

    }

    public int GetNumberByIndex(int index) {
        int kqua = -1;
        if (index < rainbow.length) {
            kqua = rainbow[index];
        }
        return kqua;

    }

}
